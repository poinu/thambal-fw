;(function(ROOT){

	ROOT.base.class("persistent", {
		/**
		payload:{
			"url" 			: "data url",
			"method"		: "request method. Default GET",
			"success"		: "Callback on success",
			"failure"		: "Callback on Error",
			"data"			: "Data to be sent to server",
			"header"		: "Header data to be sent",
			"cache"			: "Cache/persist the response data.Default value false",
			"forced"		: "If TRUE, fetched data from server. Else from cache. Default TRUE",
			"context"		: "Context for the callback"
		}
	
		*/

		onIntialize : function(){
			
		},

		load : function(payload){
			ROOT.utils.mask(true);
			var isForced = (payload.forced==null)?true:payload.forced;
			var p = payload;
			var _payload ={
				url : p.url,
				method : (p.method == null)?"GET" : p.method,
				error : p.error,
				headers 	: p["headers"],
				data 	: p.data,
				context : {
					ths : this,
					context : p.context,
					cache : (p.cache==null)?false:p.cache,
					_p 	: p
				},
				success : function(data,status,xhr){
					ROOT.utils.mask(false);
					if(this.cache == true){
						this.ths.cache(md5(p.url),data);
					}
					p.success(data,status,xhr);
				},
				error : function(data){
					ROOT.utils.mask(false);
					if(p.hasOwnProperty("failure")){
						p.failure(data);
					}
				}
			}

			if(isForced){
				$.ajax(_payload);						
			}else{
				//get data from cached if available
				var data = this.cache(md5(p.url));
				if(data != null){
					//load from ajax and store in cache					
					p.success(data,true);
					ROOT.utils.mask(false);
				}else{
					$.ajax(_payload);	
				}
			}
		},

		cache : function(name,data){
			if(data == null && name !=null){
				// RETRIVE
				var dataString = localStorage.getItem(name);
				var data = null;
				if(null != dataString){
					data = JSON.parse(dataString);
				}
				return data;
			}else if(data!= null && name !=null){
				// STORE
				localStorage.setItem(name, JSON.stringify(data));
			}else{
				// CLEAR ALL
				localStorage.clear();
			}
		},

		get : function(id){
			return JSON.parse(localStorage.getItem(id));
		},

		saveText : function(id,value){
			localStorage.setItem(md5(id),value);
		},

		getText : function(id){
			return localStorage.getItem(md5(id));
		},

		format : function(){
			localStorage.clear();
		}


	});

})(window[document.head.getAttribute("data-root")]);