;(function(ROOT){
	var compiledTemplate = {};
	var tpls = {};
	ROOT.base.class("tngine", {
		onIntialize : function(){
			tpls = ROOT.base.tpls;
			this.registerHandleBarEvent();
		},

		registerHandleBarEvent : function(){
			Handlebars.registerHelper('ifvalue', function (conditional, options) {
			  if (options.hash.value === conditional) {
			    return options.fn(this)
			  } else {
			    return options.inverse(this);
			  }
			});
		},
		
		tpl : function(id){
			return ROOT.utils.dap(id,tpls,"");
		},

		render : function(templateId, renderTo, data , isAppend){
			var template = compiledTemplate[templateId];
			if(null == template){				
				template = Handlebars.compile(this.tpl(templateId));
				compiledTemplate[templateId] = template;
			}else{
				console.log("using pre-compiled template for "+templateId);
			}
			if( true == isAppend){
				ROOT.dom.append(renderTo,template(data));
			}else{
				ROOT.dom.html(renderTo,template(data));
			}
			ROOT.trouter.publish("renderCompleted",arguments);
		}
	},null);
})(window[document.head.getAttribute("data-root")]);