;(function(ROOT){
	var _Object = {
		_classes : {},
		_templates : {},
		_class : function(_name,_obj,_base,imports){
			var baseObj = _Object._classes[_base];
			var instance = Object.assign(_obj,baseObj == null?{}:baseObj);
			instance = Object.assign(instance,("base.delegate" != _base)?_Object._classes["base.delegate"]:{});			
			_Object._classes[_name] = instance;
			//_Object.registerPageEvent(instance);
			ROOT.trouter.register(instance);
			//_Object.registerClickEvent(instance);
			if(imports!=null && Array.isArray(imports)){
				if(!instance.hasOwnProperty("imports")){
					instance["imports"] = {};
				}
				for(var i=0;i<imports.length;i++){
					instance.imports[imports[i]] = _Object._classes[imports[i]]
				}
				if(null != baseObj){
					baseObj["imports"] = instance["imports"];
				}
			}
			if(instance.hasOwnProperty("onIntialize")){
				instance.onIntialize();
			}
		},

		_template : function(_name,_obj){
			var existing = _Object._templates[_name];
			if(null != existing){
				console.log("template with same name already exists");
				return;
			}
			 _Object._templates[_name] = _obj;
		},

		/*registerPageEvent : function(obj){
			var evt = utils.dap("events.page",obj,[]);
			if(evt.length == 0 ) return;
			ROOT.trouter.subscribe("pageChange",function (e) {
				var uri = location.pathname;
			  	if(evt.includes(uri)){
			  		obj.pageEvent(uri,location.hash);
			  	}
			});
		}*/

		onReady : function(callback){			
			if(null != callback){
				ROOT.trouter.subscribe("onReady", callback);
			}
		}
	}
	var utils = {
		dap: function(path, data, defaultValue) {
	        var paths = path.split(".");
	        if (paths.length === 0) {
	            return null;
	        }
	        var currentData = data;
	        for (var index in paths) {
	            if (paths[index]) {
	                try {
	                    if (currentData && currentData.hasOwnProperty(paths[index])) {
	                        currentData = currentData[paths[index]];
	                    } else {
	                        currentData = defaultValue;
	                    }
	                } catch (e) {
	                    currentData = defaultValue;
	                }
	            }
	        }
	        if(null ==currentData){
	            currentData = defaultValue;
	        }
	        return currentData;
	    },

	    flash : function(msg,type){
	    	//Flash a message
	    	alert(msg);
	    },

	    mask : function(show){
	    	if(!dom.exists("#nong-mask")){
	    		dom.append("body","<div id='nong-mask'><div class='loader'></div></div>");
	    	}
	    	if(show){
	    		dom.rmcls("#nong-mask","hide");
    		}else{
				dom.addcls("#nong-mask","hide");
    		}
	    },

	    isEmpty : function(values){
	    	if(Array.isArray(values)){
	    		for(var i=0;i<values.length;i++){
	    			var val = values[i];
	    			if(null == val || (null !=val && val.length==0)){
	    				return true;
	    			}
	    		}
	    	}else{
	    		if(null == values || (null !=values && values.length==0)){
	    				return true;
	    			}
	    	}
	    	return false;
	    }
	}   
	var dom = {
	    show : function(id){
	    	//TODO: TO BE REPLACE WITH VANILA JS
	    	$(id).show();
	    },
	    hide : function(id){
	    	//TODO: TO BE REPLACE WITH VANILA JS
	    	$(id).hide();
	    },
	    html : function(id, content){
	    	//TODO: TO BE REPLACE WITH VANILA JS
	    	$(id).html(content);
	    },
	    append : function(id, content){
	    	$(content).appendTo(id);
	    },
	    hasCls : function(id,clsName){
	    	return $(id).hasClass(clsName);
	    },
	    addcls: function(id,clsName){
	    	$(id).addClass(clsName);
	    },
	    rmcls: function(id,clsName){
	    	$(id).removeClass(clsName);
	    },
	    isVisible : function(id){
	    	return $(id).is(":visible");
	    },

	    exists : function(id){
	    	return $(id).length != 0;
	    },

	    value : function(id,value){
	    	if(null == value){
	    		//GET
	    		return $(id).val();
	    	}else{
	    		//SET
	    		$(id).val(value);
	    	}
	    }
	}

	
	// Expose API
	ROOT.utils = utils;
	ROOT.dom = dom;
	ROOT.base = {
		class 		: _Object._class,
		_class_ 	: _Object._classes,
		template 	: _Object._template,
		tpls 		: _Object._templates
	};
	ROOT.l = function(msg){
			if(msg instanceof Object){
				console.table(msg);
			}
			console.log(arguments);
			
	};
	
})(window[document.head.getAttribute("data-root")]);

window.onload = function(){
	window[document.head.getAttribute("data-root")].trouter.publish("onReady");
}