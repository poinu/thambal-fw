;(function(ROOT){
	var _uri = "/";
	var _hash = null;
	var router = {
		event : null,
		cache : {},
		init : function(){
			$(window).on("popstate", this.onHashChange.bind(this));
			this.registerEvent('onReady', this.onHashChange,this);
			this.registerNavigateLink();
		},

		registerNavigateLink : function(){
			$( document ).on( "click", ".link", function(event){
				var link = event.currentTarget.getAttribute("data-link");
				if(null != link){
					this.changePage(link);
				}		
			}.bind(this));
		},
		
		newEvent : function(name, data){
			if(window.hasOwnProperty("CustomEvent")){
				return new CustomEvent(name, data);
			}
			console.log("CustomEvent not supported by this browser");
			var event = document.createEvent('Event');
			event.initEvent(name, true, true);
			return event;
		},

		onHashChange : function(event){
			var extra = event;
			if(null != event && event.hasOwnProperty("originalEvent") && event.originalEvent.hasOwnProperty("state")){
				extra = event.originalEvent.state;
			}
			var data = {
				"context" : this,
				"name" : "pageChange",
				"uri" : location.pathname,
				"hash": location.hash,
				"payload" : extra
			}
			this.triggerEvent("pageChange",data);
		},

		registerEvent : function(topic,callback,context){			
			if(!this.cache[topic]){
	            this.cache[topic] = [];
	        }
	        this.cache[topic].push(callback.bind(context));
			},

		triggerEvent : function(topic,data){
			//document.dispatchEvent(this.newEvent(name),data);
			this.cache[topic] && $.each(this.cache[topic], function(){
	            this.apply(data);
	        });
		},

		changePage : function(uri,data){
			if(true == data){
				location.href=uri;
				return;
			}
			var state = data,
		        title = "Page title",
		        path  = uri;
    			history.pushState(state, title, path);
    			window.dispatchEvent(new Event("popstate"));
		},

		register : function(obj){
			this.registerPageEvent(obj);
			this.registerClickEvent(obj, "click");
			this.registerClickEvent(obj, "dblclick");
			this.registerClickEvent(obj, "mouseover");
			this.registerClickEvent(obj, "keyup");
		},

		registerClickEvent : function(obj, ename){
			var _obj = obj;
			var evts = ROOT.utils.dap("events."+ename,obj,null);
			if(evts == null ) return;
			var keys = Object.keys(evts);
			for(var i=0;i<keys.length;i++){
				var key = keys[i];
				//TODO: TO BE REPLACE WITH PLAIN JS
				$( document ).on( ename, key, function(event){
					_obj[evts[event.handleObj.selector]](event);
				});
			}
		},
		/**
		* Register url page 
		*/
		registerPageEvent : function(obj){
			var evt = ROOT.utils.dap("events.page",obj,[]);
			if(evt.length == 0 ) return;
			this.registerEvent("pageChange",function (e) {
				var evt = ROOT.utils.dap("events.page",this,[]);
				var uri = location.pathname;				
				_uri=uri;
				_hash = location.hash;
				if(evt.includes(uri)){
					_uri=uri;
					_hash = location.hash;
			  		this.pageEvent(uri,location.hash);
			  		return;
			  	}
			  	//Check for wild char. support only ending with *
				if(evt.join(",").indexOf("*") != -1){
					for(var i=0;i<evt.length;i++){
						if(evt[i].indexOf("*")!=-1){
							var path = evt[i].replace("*","");
							if(uri.indexOf(path) == 0){								
								this.pageEvent(uri,location.hash);
								return;
							}
						}
					}
				}

				//Check for wild char. support only ending with #
				if(evt.join(",").indexOf("#") != -1){
					for(var i=0;i<evt.length;i++){
						if(evt[i].indexOf("#")!=-1){
							var path = evt[i].replace("#","");
							if(uri.indexOf(path) == 0){								
								this.pageEvent(uri,location.hash);
								return;
							}
						}
					}
				}
		//	ROOT.trouter.publish("PAGE_404");	
			}.bind(obj));
		},

		currentPage : function(){
			return {
			uri 	: _uri,
			hash 	: _hash
		
			}
		}
	}
	
	router.init();
	ROOT.trouter = {
		navigate 	: router.changePage.bind(router),
		subscribe 	: router.registerEvent.bind(router),
		publish 	: router.triggerEvent.bind(router),
		register 	: router.register.bind(router),
		current		: router.currentPage
	};
})(window[document.head.getAttribute("data-root")]);
;(function(ROOT){
	var _Object = {
		_classes : {},
		_templates : {},
		_class : function(_name,_obj,_base,imports){
			var baseObj = _Object._classes[_base];
			var instance = Object.assign(_obj,baseObj == null?{}:baseObj);
			instance = Object.assign(instance,("base.delegate" != _base)?_Object._classes["base.delegate"]:{});			
			_Object._classes[_name] = instance;
			//_Object.registerPageEvent(instance);
			ROOT.trouter.register(instance);
			//_Object.registerClickEvent(instance);
			if(imports!=null && Array.isArray(imports)){
				if(!instance.hasOwnProperty("imports")){
					instance["imports"] = {};
				}
				for(var i=0;i<imports.length;i++){
					instance.imports[imports[i]] = _Object._classes[imports[i]]
				}
				if(null != baseObj){
					baseObj["imports"] = instance["imports"];
				}
			}
			if(instance.hasOwnProperty("onIntialize")){
				instance.onIntialize();
			}
		},

		_template : function(_name,_obj){
			var existing = _Object._templates[_name];
			if(null != existing){
				console.log("template with same name already exists");
				return;
			}
			 _Object._templates[_name] = _obj;
		},

		/*registerPageEvent : function(obj){
			var evt = utils.dap("events.page",obj,[]);
			if(evt.length == 0 ) return;
			ROOT.trouter.subscribe("pageChange",function (e) {
				var uri = location.pathname;
			  	if(evt.includes(uri)){
			  		obj.pageEvent(uri,location.hash);
			  	}
			});
		}*/

		onReady : function(callback){			
			if(null != callback){
				ROOT.trouter.subscribe("onReady", callback);
			}
		}
	}
	var utils = {
		dap: function(path, data, defaultValue) {
	        var paths = path.split(".");
	        if (paths.length === 0) {
	            return null;
	        }
	        var currentData = data;
	        for (var index in paths) {
	            if (paths[index]) {
	                try {
	                    if (currentData && currentData.hasOwnProperty(paths[index])) {
	                        currentData = currentData[paths[index]];
	                    } else {
	                        currentData = defaultValue;
	                    }
	                } catch (e) {
	                    currentData = defaultValue;
	                }
	            }
	        }
	        if(null ==currentData){
	            currentData = defaultValue;
	        }
	        return currentData;
	    },

	    flash : function(msg,type){
	    	//Flash a message
	    	alert(msg);
	    },

	    mask : function(show){
	    	if(!dom.exists("#nong-mask")){
	    		dom.append("body","<div id='nong-mask'><div class='loader'></div></div>");
	    	}
	    	if(show){
	    		dom.rmcls("#nong-mask","hide");
    		}else{
				dom.addcls("#nong-mask","hide");
    		}
	    },

	    isEmpty : function(values){
	    	if(Array.isArray(values)){
	    		for(var i=0;i<values.length;i++){
	    			var val = values[i];
	    			if(null == val || (null !=val && val.length==0)){
	    				return true;
	    			}
	    		}
	    	}else{
	    		if(null == values || (null !=values && values.length==0)){
	    				return true;
	    			}
	    	}
	    	return false;
	    }
	}   
	var dom = {
	    show : function(id){
	    	//TODO: TO BE REPLACE WITH VANILA JS
	    	$(id).show();
	    },
	    hide : function(id){
	    	//TODO: TO BE REPLACE WITH VANILA JS
	    	$(id).hide();
	    },
	    html : function(id, content){
	    	//TODO: TO BE REPLACE WITH VANILA JS
	    	$(id).html(content);
	    },
	    append : function(id, content){
	    	$(content).appendTo(id);
	    },
	    hasCls : function(id,clsName){
	    	return $(id).hasClass(clsName);
	    },
	    addcls: function(id,clsName){
	    	$(id).addClass(clsName);
	    },
	    rmcls: function(id,clsName){
	    	$(id).removeClass(clsName);
	    },
	    isVisible : function(id){
	    	return $(id).is(":visible");
	    },

	    exists : function(id){
	    	return $(id).length != 0;
	    },

	    value : function(id,value){
	    	if(null == value){
	    		//GET
	    		return $(id).val();
	    	}else{
	    		//SET
	    		$(id).val(value);
	    	}
	    }
	}

	
	// Expose API
	ROOT.utils = utils;
	ROOT.dom = dom;
	ROOT.base = {
		class 		: _Object._class,
		_class_ 	: _Object._classes,
		template 	: _Object._template,
		tpls 		: _Object._templates
	};
	ROOT.l = function(msg){
			if(msg instanceof Object){
				console.table(msg);
			}
			console.log(arguments);
			
	};
	
})(window[document.head.getAttribute("data-root")]);

window.onload = function(){
	window[document.head.getAttribute("data-root")].trouter.publish("onReady");
}

///////////////////////////////////
;(function(){
//*********************SET ROOT FROM HEAD OBJECT*********************************
var ROOT = window[document.head.getAttribute("data-root")];
//*********************END-ROOT FROM HEAD OBJECT*********************************

ROOT.base.class("base.delegate", {
	_init: function(){
		console.log("init")
	},

	alias : function(name){
		// Get the imported component
		return ROOT.utils.dap("imports."+name,this,{});
	}
});

})();
////////////////////////////////////////
;(function(ROOT){

	ROOT.base.class("persistent", {
		/**
		payload:{
			"url" 			: "data url",
			"method"		: "request method. Default GET",
			"success"		: "Callback on success",
			"failure"		: "Callback on Error",
			"data"			: "Data to be sent to server",
			"header"		: "Header data to be sent",
			"cache"			: "Cache/persist the response data.Default value false",
			"forced"		: "If TRUE, fetched data from server. Else from cache. Default TRUE",
			"context"		: "Context for the callback"
		}
	
		*/

		onIntialize : function(){
			
		},

		load : function(payload){
			ROOT.utils.mask(true);
			var isForced = (payload.forced==null)?true:payload.forced;
			var p = payload;
			var _payload ={
				url : p.url,
				method : (p.method == null)?"GET" : p.method,
				error : p.error,
				headers	: p["headers"],
				data 	: p.data,
				contentType: p.contentType,
				context : {
					ths : this,
					context : p.context,
					cache : (p.cache==null)?false:p.cache,
					_p 	: p
				},
				success : function(data,status,xhr){
					ROOT.utils.mask(false);
					if(this.cache == true){
						this.ths.cache(md5(p.url),data);
					}
					p.success(data,status,xhr);
				},
				error : function(data){
					ROOT.utils.mask(false);
					if(p.hasOwnProperty("failure")){
						p.failure(data);
					}
				}
			}

			if(isForced){
				$.ajax(_payload);						
			}else{
				//get data from cached if available
				var data = this.cache(md5(p.url));
				if(data != null){
					//load from ajax and store in cache					
					p.success(data,true);
					ROOT.utils.mask(false);
				}else{
					$.ajax(_payload);	
				}
			}
		},

		cache : function(name,data){
			if(data == null && name !=null){
				// RETRIVE
				var dataString = localStorage.getItem(name);
				var data = null;
				if(null != dataString){
					data = JSON.parse(dataString);
				}
				return data;
			}else if(data!= null && name !=null){
				// STORE
				localStorage.setItem(name, JSON.stringify(data));
			}else{
				// CLEAR ALL
				localStorage.clear();
			}
		},

		get : function(id){
			return JSON.parse(localStorage.getItem(id));
		},

		saveText : function(id,value){
			localStorage.setItem(md5(id),value);
		},

		getText : function(id){
			return localStorage.getItem(md5(id));
		},

		format : function(){
			localStorage.clear();
		}


	});

})(window[document.head.getAttribute("data-root")]);
//////////////////////////////
;(function(ROOT){
	var compiledTemplate = {};
	var tpls = {};
	ROOT.base.class("tngine", {
		onIntialize : function(){
			tpls = ROOT.base.tpls;
			this.registerHandleBarEvent();
		},

		registerHandleBarEvent : function(){
			Handlebars.registerHelper('ifvalue', function (conditional, options) {
			  if (options.hash.value === conditional) {
			    return options.fn(this)
			  } else {
			    return options.inverse(this);
			  }
			});
		},
		
		tpl : function(id){
			return ROOT.utils.dap(id,tpls,"");
		},

		render : function(templateId, renderTo, data , isAppend){
			var template = compiledTemplate[templateId];
			if(null == template){				
				template = Handlebars.compile(this.tpl(templateId));
				compiledTemplate[templateId] = template;
			}else{
				console.log("using pre-compiled template for "+templateId);
			}
			if( true == isAppend){
				ROOT.dom.append(renderTo,template(data));
			}else{
				ROOT.dom.html(renderTo,template(data));
			}
			ROOT.trouter.publish("renderCompleted",arguments);
		}
	},null);
})(window[document.head.getAttribute("data-root")]);
////////////////////////

/////////////////////
;(function(ROOT){
	ROOT.base.class("tui", {
		onIntialize : function(){
			tpls = ROOT.base.tpls;
		},
		
		menu : function(payload){
			var _payload = {
				
			}
		}
	});
})(window[document.head.getAttribute("data-root")]);