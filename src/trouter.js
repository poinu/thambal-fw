;(function(ROOT){
	var _uri = "/";
	var _hash = null;
	var router = {
		event : null,
		cache : {},
		init : function(){
			$(window).on("popstate", this.onHashChange.bind(this));
			this.registerEvent('onReady', this.onHashChange,this);
			this.registerNavigateLink();
		},

		registerNavigateLink : function(){
			$( document ).on( "click", ".link", function(event){
				var link = event.currentTarget.getAttribute("data-link");
				if(null != link){
					this.changePage(link);
				}		
			}.bind(this));
		},
		
		newEvent : function(name, data){
			if(window.hasOwnProperty("CustomEvent")){
				return new CustomEvent(name, data);
			}
			console.log("CustomEvent not supported by this browser");
			var event = document.createEvent('Event');
			event.initEvent(name, true, true);
			return event;
		},

		onHashChange : function(event){
			var extra = event;
			if(null != event && event.hasOwnProperty("originalEvent") && event.originalEvent.hasOwnProperty("state")){
				extra = event.originalEvent.state;
			}
			var data = {
				"context" : this,
				"name" : "pageChange",
				"uri" : location.pathname,
				"hash": location.hash,
				"payload" : extra
			}
			this.triggerEvent("pageChange",data);
		},

		registerEvent : function(topic,callback,context){			
			if(!this.cache[topic]){
	            this.cache[topic] = [];
	        }
	        this.cache[topic].push(callback.bind(context));
			},

		triggerEvent : function(topic,data){
			//document.dispatchEvent(this.newEvent(name),data);
			this.cache[topic] && $.each(this.cache[topic], function(){
	            this.apply(data);
	        });
		},

		changePage : function(uri,data){
			if(true == data){
				location.href=uri;
				return;
			}
			var state = data,
		        title = "Page title",
		        path  = uri;
    			history.pushState(state, title, path);
    			window.dispatchEvent(new Event("popstate"));
		},

		register : function(obj){
			this.registerPageEvent(obj);
			this.registerClickEvent(obj, "click");
			this.registerClickEvent(obj, "dblclick");
			this.registerClickEvent(obj, "mouseover");
			this.registerClickEvent(obj, "keyup");
		},

		registerClickEvent : function(obj, ename){
			var _obj = obj;
			var evts = ROOT.utils.dap("events."+ename,obj,null);
			if(evts == null ) return;
			var keys = Object.keys(evts);
			for(var i=0;i<keys.length;i++){
				var key = keys[i];
				//TODO: TO BE REPLACE WITH PLAIN JS
				$( document ).on( ename, key, function(event){
					_obj[evts[event.handleObj.selector]](event);
				});
			}
		},
		/**
		* Register url page 
		*/
		registerPageEvent : function(obj){
			var evt = ROOT.utils.dap("events.page",obj,[]);
			if(evt.length == 0 ) return;
			this.registerEvent("pageChange",function (e) {
				var evt = ROOT.utils.dap("events.page",this,[]);
				var uri = location.pathname;				
				_uri=uri;
				_hash = location.hash;
				if(evt.includes(uri)){
					_uri=uri;
					_hash = location.hash;
			  		this.pageEvent(uri,location.hash);
			  		return;
			  	}
			  	//Check for wild char. support only ending with *
				if(evt.join(",").indexOf("*") != -1){
					for(var i=0;i<evt.length;i++){
						if(evt[i].indexOf("*")!=-1){
							var path = evt[i].replace("*","");
							if(uri.indexOf(path) == 0){								
								this.pageEvent(uri,location.hash);
								return;
							}
						}
					}
				}

				//Check for wild char. support only ending with #
				if(evt.join(",").indexOf("#") != -1){
					for(var i=0;i<evt.length;i++){
						if(evt[i].indexOf("#")!=-1){
							var path = evt[i].replace("#","");
							if(uri.indexOf(path) == 0){								
								this.pageEvent(uri,location.hash);
								return;
							}
						}
					}
				}
		//	ROOT.trouter.publish("PAGE_404");	
			}.bind(obj));
		},

		currentPage : function(){
			return {
			uri 	: _uri,
			hash 	: _hash
		
			}
		}
	}
	
	router.init();
	ROOT.trouter = {
		navigate 	: router.changePage.bind(router),
		subscribe 	: router.registerEvent.bind(router),
		publish 	: router.triggerEvent.bind(router),
		register 	: router.register.bind(router),
		current		: router.currentPage
	};
})(window[document.head.getAttribute("data-root")]);